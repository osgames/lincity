Lincity - A City Simulation Game
================================

Welcome
-------

Lincity is a city simulation game for SVGALIB, X Windows and 
Microsoft Windows.
The current stable release of lincity 
is version 1.12.1 (Aug. 13, 2004).

The current development (unstable) release of lincity is 
version 1.13.1 (July 7, 2003).

icon:splash.jpg[Image of lincity splash screen]

What is Lincity?
----------------
Lincity is a city simulation game.  You are required to 
build and maintain a city. You must feed, house, provide jobs and 
goods for your residents. You can build a sustainable economy 
with the help of renewable energy and recycling, or you can go for 
broke and build rockets to escape from a pollution ridden and 
resource starved planet, it's up to you.

Supported platforms
~~~~~~~~~~~~~~~~~~~
As best we know, lincity works well on the following platforms.

X11R6::
Linux, FreeBSD, AIX, Solaris, HP-UX, IRIX, OS X, SCO, Tru64 and CYGWIN
SVGAlib::
Linux
Win32::
Win95, Win98, ME, NT, 2000, and XP

In addition, lincity works on the following platforms through an 
X Windows emulation layer.

Other Platforms::
BeOS, OpenVMS, OS/2, AtheOS

Screenshots
~~~~~~~~~~~
These screenshots are from version 1.12pre48.

- link:screenshots/index.html[Lincity screenshots]

Getting help
------------

For information on obtaining, installing and running 
lincity, please refer to the current README file.

- link:readme.html[README file]

Basic information on how to play the game can be found in the 
extensive on-line help.  Right click almost anywhere to 
get help.

Forums
~~~~~~

Please use our forums on the sourceforge site.  

- http://sourceforge.net/forum/?group_id=33390[Forums at sourceforge]

Email
~~~~~

Email support is offered through a low-traffic email list.

- mailto:lincity-users@lists.sourceforge.net[]

Or, you can email to Greg or Corey 
directly.  
If you do this, please put "lincity" in
the subject line to reduce the chance your email 
will be deleted as SPAM.

- mailto:gregsharp@geocities.com[]
- mailto:immunizer@users.sourceforge.net[]

Links
~~~~~

- The original lincity page: http://www.floot.demon.co.uk/lincity.html[]
- The original lincity win32 page: http://www.geocities.com/gregsharp.geo/lincity.html[]

Downloads
---------

*Source Code*

- http://www.sourceforge.net/projects/lincity[]
- http://www.ibiblio.org/pub/Linux/games/strategy/lincity-1.12.0.tar.gz[]

*Win32 Binary Packages*

- http://www.sourceforge.net/projects/lincity[]
- http://www.winsite.com/bin/Info?19000000037887[]
- http://www.simtel.net/pub/pd/74848.html[]

You can also get the windows version 
as part of Philip White's 
very nice windows freeware collection.
Available as CD or bittorent: 

-  http://pmw.myip.org/oss/[]

*Unix Binary Packages*

Here are the binary packages that I know of.  Please email 
for additions and corrections.
A special thanks to all of the UNIX binary maintainers out there!

- Autopackage: http://prdownloads.sourceforge.net/lincity/lincity-1.13.1.x86.package?download[]
- Cygwin: http://sourceforge.net/projects/cygwin-ports/[]
- FreeBSD: http://www.freshports.org/games/lincity/[]
- HPUX: http://hpux.cs.utah.edu/hppd/hpux/Games/Arcade/alpha.html[]
- IRIX: http://www.nekochan.net/downloads/[]
- Linux/Debian: http://packages.debian.org/lincity[]
- Linux/RPM: http://rpmfind.net/linux/rpm2html/search.php?query=lincity[]
- OS X: http://fink.sourceforge.net/pdb/package.php/lincity[]

*Non-Unix Ports*

Lincity also runs on several non-UNIX platforms through X Windows
emulation layers.  Outstanding!

- AtheOS: http://kamidake.other-space.com/display_entry.php?id=80[]
- BeOS: http://www.bebits.com/app/1859[]
- OpenVMS: http://decwarch.free.fr/strategy.html[]
- OS/2: http://homepages.tu-darmstadt.de/~st002279/os2/html/xfreeos2.html[]
- OS/2: http://hobbes.nmsu.edu/cgi-bin/h-browse?dir=/pub/os2/games/strategy[]

Note that BeOS version 1.111 and AtheOS version 1.12 are 
based on lincity 1.11.

Copyright
---------
Lincity is copyrighted software.  
You may freely copy, distribute and modify this program in any form, 
provided you adhere to the conditions specified 
in the GNU General Public License.
A copy of this license is included with the game.

Copyright (c) I J Peters 1995-1997. +
Copyright (c) Greg Sharp 1997-2003. +
Copyright (c) Corey Keasling 2000-2003. +

In addition, thanks to 
link:Acknowledgements[all the many people] 
who have sent in bug reports and patches.

Kudos
-----

- This page was brought to you through 
	generous assistance provided by http://sf.net[Sourceforge].
- Thanks to http://www.granitecanyon.com/[Granite Canyon] for serving DNS for lincity.org.
